# coding=utf-8
from setuptools import setup
from os import path

__author__ = 'Arlena <arlena@hubsec.eu>'
__version__ = '0.2.1'

base_dir = path.abspath(path.dirname(__file__))

setup(
    name='afmarkup',
    version=__version__,
    description=u'Simple markup tool',
    author='Arlena Derksen',
    author_email='arlena@hubsec.eu',
    license='MIT',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2.7'
    ],
    keywords='markup',
    packages=['afmarkup']
)
