# coding=utf-8
import abc

__author__ = 'Arlena <arlena@hubsec.eu>'


"""
Markup: u'This is a text and this needs to be [MU]marked[/MU]'
BaseStyle
"""


class BaseStyle(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self):
        self.placeholder = '{{text}}'

        # self.markup = {
        #     'id': {'combination': ('tag_open', 'tag_close'),
        #            'style': u'start-placeholder-end',
        #            'comment': None
        #            },
        #     '...': '...'
        # }

        self.markup = dict()

    def __repr__(self):
        return u'{0}: #Combinations: {1}'.format(self.__class__.__name__, self.n_combinations)

    def add_combination(self, combination, style, comment=None):
        """
        Adds tags and actions to style
        :param combination: start and end tag in tuple (length: 2)
        :param style: style to replace tags with including placeholder
        :param comment: comment for this combination, defaults to None
        :return: combination identifier, needed when removing
        :raises MarkupCombinationException: if parameter combination is not a tuple
        :raises MarkupTagsInvalidException: if parameter combination has not exactly 2 values
        """
        if not isinstance(combination, tuple):
            raise MarkupCombinationException
        if len(combination) != 2:
            raise MarkupTagsInvalidException
        combination_id = self.n_combinations + 1

        self.markup[str(combination_id)] = {'combination': combination,
                                            'style': style,
                                            'comment': comment}
        return combination_id

    def remove_combination(self, combination_id):
        """
        Remove tags and action from style
        :param combination_id: combination id as known in markup dictionary
        :raises MarkupCombinationNotFoundException: if combination_id not in markup dictionary
        """
        if combination_id in self.markup.keys():
            del self.markup[combination_id]
        else:
            raise MarkupCombinationNotFoundException(combination_id)

    @property
    def n_combinations(self):
        return len(self.markup)


class SampleStyle(BaseStyle):
    def __init__(self):
        super(SampleStyle, self).__init__()
        self.add_combination(('[MU]', '[/MU]'),
                             u'<span style="background-color: #FFFF00">{0}</span>'.format(self.placeholder))
        self.add_combination(('[MU2]', '[/MU2]'),
                             u'<span style="background-color: #CCFF66">{0}</span>'.format(self.placeholder))


class MarkupCombinationException(Exception):
    def __init__(self):
        self.msg = u'Invalid markup combination. Expected type: tuple'

    def __str__(self):
        return repr(self.msg)


class MarkupTagsInvalidException(Exception):
    def __init__(self):
        self.msg = u'Invalid markup tags. Expected tuple with length of 2 containing start and end tags'

    def __str__(self):
        return repr(self.msg)


class MarkupCombinationNotFoundException(Exception):
    def __init__(self, value):
        self.msg = u'Markup identifier `{0}` not found.'.format(value)

    def __str__(self):
        return repr(self.msg)
