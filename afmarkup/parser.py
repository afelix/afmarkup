# coding=utf-8
from afmarkup.models import BaseStyle, SampleStyle

__author__ = 'Arlena <arlena@hubsec.eu>'


class Parser(object):
    def __init__(self, style=None):
        if style is not None and isinstance(style, BaseStyle):
            self._style = style
        else:
            self._style = SampleStyle()

    def parse(self, text, keep_comments=False):
        comments = []
        for id_, markup in self._style.markup.iteritems():
            current_pos = 0
            while text.find(markup['combination'][0], current_pos) != -1:
                try:
                    pos_start = text.index(markup['combination'][0], current_pos)
                except ValueError:
                    pass
                else:
                    try:
                        pos_end = text.index(markup['combination'][1], pos_start)
                    except ValueError:
                        print('Malformed command: no end tag found')
                        break
                    else:
                        to_replace = text[pos_start + len(markup['combination'][0]):pos_end]
                        to_replace_full = markup['combination'][0] + to_replace + markup['combination'][1]
                        marked_up_part = markup['style'].replace(self._style.placeholder, to_replace)
                        text = text.replace(to_replace_full, marked_up_part)
                        if keep_comments and markup['comment'] is not None:
                            comments.append(markup['comment'])
        if keep_comments:
            return text, comments
        else:
            return text

if __name__ == '__main__':
    test_markup = u'This is a [MU]test[/MU] and this is [MU2]another[/MU2] [MU]test[/MU].'
    parser = Parser()
    print(parser.parse(test_markup))
